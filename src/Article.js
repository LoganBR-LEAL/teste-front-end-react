import React from 'react';
import plus from './plus-circle-solid.svg';
import './App.css';


class Article extends React.Component{
    render(){
        return(
       <div className="geral">
           <div className="video">
  <video className="controls" width="920" height="440" controls="controls" autoplay="autoplay">
    <source src="" type="video/mp4" />
    O seu navegador não suporta a tag vídeo
  </video>
  </div>
  <div className="bloco3">
    <ul className="etapas">
        <li>introdução</li>
        <li>variaveis</li>
        <li>If</li>
        <li>Else</li>
        <li>while e do while</li>
        <li>Funções em C</li>
        <li>Funções em C pt2</li>
        <li>Avançando em C</li>
        <li>Avançando em C pt2</li>
        <br></br>
        <button className="btpdf" type="button"  style={{backgroundColor:"purple", color: 'white'}}>
        <img className="" src={plus} alt="" width="20" height="20"/>
             Baixar pdf dessa atividade
        </button>
    </ul>
  </div>
       </div>
        );
    }
}

export default Article;