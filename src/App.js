import React from 'react';
import Headers from './Headers';
import Article from './Article';

function App() {
  return (
  <div>
    <Headers/>
    <Article/>
  </div>            
  );
}

export default App;
