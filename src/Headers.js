import React from 'react';
import bell from './bell-solid.svg';
import cog from './cog-solid.svg';
import coins from './coins-solid.svg';
import campus from './campusmaker.svg';
import potion from './potion.svg';
import gamep from './gamepad-solid.svg';
import grin from './grin-regular.svg';
import game from './gamepad.svg';
import c from './simbolo-do-C.svg';
import fire from './fire-solid.svg';
import './App.css';
import { Progress } from 'reactstrap';


class Headers extends React.Component{
    render(){
        return(
            <div className="geral">
            <a className="" href="">
            <img data-toggle="tooltip" data-placement="right" title="57 coins" className="coins" src={coins} width="20" height="20" alt=""/>
            </a>
            <a className="" href="">
            <img className="bell" src={bell} width="20" height="20" alt=""/>
            </a>
            <a className="" href="">
            <img className="cog" src={cog} width="20" height="20" alt=""/>
            </a>
            <div className="dropdown">
                <button type="button" className="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src={campus} width="auto" height="auto" alt="" loading="lazy"/>
                </button>
                <div className="dropdown-menu">
                    <a className="dropdown-item" href="#">Perfil</a>
                    <a className="dropdown-item" href="#">Deslogar</a>
                </div>
              </div>
            <div className="sideboard">
                <a className="navbar-brand" href="#">
                  <img className="imgbase" src={potion} width="30" height="30" alt="" loading="lazy"/>
                </a>
                <a className="navbar-brand" href="">
                    <img className="imgbase2" src={gamep} width="30" height="30" alt="" loading="lazy"/>
                </a>
                <a className="navbar-brand" href="">
                    <img className="imgbase3" src={grin} width="30" height="30" alt="" loading="lazy"/>
                </a>
                <a className="navbar-brand" href="">
                    <img className="imgbase4" src={game} width="40" height="40" alt="" loading="lazy"/>
                </a>
            </div>
          <div className="bloco1">
              <img className="c" src={c} alt="simbolo do C" width="40" height="40"/>
           <h1 className="introducao">Introdução a C</h1>
           <p className="progresso">Progresso na fase</p>
           <div className="progress">
           <div class="progress-bar" role="progressbar" style={{width: '20%',backgroundColor: 'blueviolet'}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">30%</div>
          </div>
          <div className="bloco2">
           <img className="fire" src={fire} alt="simbolo de missão" width="40" height="40"/>
           <h1 className="missao">Missões Novas</h1>
           <p className="missao2">FASE 1-Produzir um website</p>
          </div>
        </div>
    </div>
        );
    }
}

export default Headers;